import React from 'react';
import { useSelector } from 'react-redux';
import './app.css';
import Navbar from './../navbar';
import {
  Route,
  Switch,
  Redirect
} from "react-router-dom"
import ExchangeRates from '../../pages/exchange-rates';
import Chart from '../../pages/chart';
import Auth from '../../pages/auth/auth';
import ReceivingCurrency from '../../pages/receiving-currency/receiving-currency';
import CurrencyExchange from '../../pages/currency-exchange/currency-exchange';

const App = () => {
  const isAuth = useSelector(({authReducer}) => authReducer.isAuth);

  if(isAuth){
    return (   
      <>
        <Navbar />
        <div className="content">
          <Switch>
            <Route path='/exchange-rates' component={ExchangeRates} />
            <Route path='/chart' component={Chart} />
            <Route path='/receiving-currency' component={ReceivingCurrency} />
            <Route path='/currency-exchange' component={CurrencyExchange} />
            <Redirect from='/' to='/exchange-rates'/>
            {/* <Route path="*" component={NotFound} /> */}
          </Switch>
        </div>
        
      </>
    )
  }

  return (
    <Switch>
      <Route path='/auth' component={Auth} />
      <Redirect from='/' to='/auth'/>
    </Switch>
  )
  
}

export default App;
