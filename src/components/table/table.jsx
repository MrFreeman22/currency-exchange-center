import React from 'react';
import './table.css';

const Table = ({ currencies }) => {
  return (
    <table className='table'>
      <thead className='thead-light'>
        <tr>
          <th scope='col'>#</th>
          <th scope='col'>Буквенный код</th>
          <th scope='col'>Наименование</th>
          <th scope='col'>Кол-во иностранной валюты</th>
          <th scope='col'>Курс в BYN</th>
          <th scope='col'>Дата и время обновления</th>
        </tr>
      </thead>
      <tbody>
        {
          currencies.map((currency, i) => {
            return (
              <tr key={currency.Cur_ID}>
                <th scope='row'>{i+1}</th>
                <td>{currency.Cur_Abbreviation}</td>
                <td>{currency.Cur_Name}</td>
                <td>{currency.Cur_Scale}</td>
                <td>{currency.Cur_OfficialRate}</td>
                <td>{currency.Date}</td>
              </tr>
            )
          })
        }   
      </tbody>
    </table>
  )
}

export default Table;