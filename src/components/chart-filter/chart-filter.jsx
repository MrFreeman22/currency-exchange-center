import React from 'react';
import './chart-filter.css';
import { useDispatch, useSelector } from 'react-redux';

import { getCourseDynamicData } from './../../redux/reducers/chartReducer';
import { getAllCurrencies } from './../../redux/reducers/currenciesReducer';

import { chartActions } from './../../redux/actions';

import { converteDateToMS } from './../../validators/chart-filter-validator';



const maxEndDate = (startDate, maxTimeSpan, currenDate) => {
  const maxDateMS = +converteDateToMS(startDate) + +maxTimeSpan;
  if(maxDateMS > converteDateToMS(currenDate)) return currenDate;
  const m = new Date(maxDateMS);
  return `${m.getFullYear()}-${m.getMonth()+1}-${m.getDate()}`;
}


const ChartFilter = () => {
  const dispatch = useDispatch();
  const currentDate = `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`;

  const currencies = useSelector(({currenciesReducer}) => currenciesReducer.currencies);
  const { minDate, selectedCurrencyId, startDate, endDate, maxTimeSpan } = useSelector(({chartReducer}) => chartReducer);

  if(currencies.length === 0) dispatch(getAllCurrencies());
  return (
    <div className='chart-filter'>
      <select className='custom-select select-currency'
              value={selectedCurrencyId}
              onChange={e => {
                dispatch(chartActions.setSelectedCurrencyChartFilter(e.target.value));
                dispatch(getCourseDynamicData());
               }}
      >
        <option value='' disabled>Выберите валюту</option>
        {
          currencies.map(currency => {
            return <option key={currency.Cur_ID} value={currency.Cur_ID}>{currency.Cur_Name}</option>
          })
        }
      </select>

      <div className='start-date'>
        <label>От:</label>
        <input type='date' 
               className='start-date__inp' 
               value={startDate}
               min={minDate} 
               max={currentDate}
               onChange={e => {
                dispatch(chartActions.setStartDate(e.target.value));
                dispatch(getCourseDynamicData());
               }} />
      </div>

      <div className='end-date'>
        <label>До:</label>
        <input type='date' 
               className='end-date__inp'
               value={endDate} 
               min={startDate} 
               max={maxEndDate(startDate, maxTimeSpan, currentDate)}
               onChange={e => {
                dispatch(chartActions.setEndDate(e.target.value));
                dispatch(getCourseDynamicData());
               }} />
      </div>
    </div>
  )
}

export default ChartFilter;