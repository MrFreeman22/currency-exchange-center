import React from 'react';
import './navbar.css';
import { NavLink } from 'react-router-dom';


const Navbar = () => {
    return (
        <nav className='navbar-container'>
            <ul className='navbar-list'>
                <li className='navbar-list__item'>
                    <NavLink to='/exchange-rates'>Курсы валют</NavLink>
                </li>
                <li className='navbar-list__item'>
                    <NavLink to='/receiving-currency'>Пополнение валюты</NavLink>
                </li>
                <li className='navbar-list__item'>
                    <NavLink to='/currency-exchange'>Обмен валюты</NavLink>
                </li>
                <li className='navbar-list__item'>
                    <NavLink to='/chart'>График валюты</NavLink>
                </li>
            </ul>
        </nav>
    )  
}

export default Navbar;