import { isNumber } from './isNumber';

export const currencyExchangeValidator = (amount, available) => {
  if(!isNumber(+amount)) return false;
  if(+amount > +available) return false;
  //if(+amount > 0) return true;
  return true;
}