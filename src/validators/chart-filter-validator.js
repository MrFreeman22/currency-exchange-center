export const converteDateToMS = date => {
  const tmp = new Date(date);
  return +tmp;
}

export const chartFilterValidator = (minDate, maxTimeSpan, startDate, endDate, selectedCurrency) => {
  if(startDate === '') return false;
  if(endDate === '') return false;
  if(selectedCurrency === '') return false;
  if(converteDateToMS(minDate) > converteDateToMS(startDate)) return false;
  if((converteDateToMS(endDate) - converteDateToMS(startDate)) > maxTimeSpan) return false;
  return true;
}

