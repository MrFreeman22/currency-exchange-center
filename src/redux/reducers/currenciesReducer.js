import { currenciesAT } from './../actions-types';
import { currenciesActions } from './../actions';
import BankApi from './../../api/bank-api';

const bankApi = new BankApi();

const inicialState = {
  currencies: []
};

export const getAllCurrencies = () => async dispatch => {
  try {
    const data = await bankApi.getAllCurrencies();
    if(data.status === 200) dispatch(currenciesActions.setAllCurrencies(data.data));
  } catch (error) {
    if(error.message === 'Network Error'){
      alert('Отсутствует подключение к интернету!');
    }
  }
}

const currenciesReducer = (state = inicialState, action) => {
  switch(action.type){
    case currenciesAT.SET_ALL_CURRENCIES: 
      return {
        ...state,
        currencies: action.currencies
      }

    default: return state;
  }
}

export default currenciesReducer;