import { authAT } from './../actions-types';
import { authActions } from './../actions';
import Api from './../../api/api';

const api = new Api();

const inicialState = {
  isAuth: false,
  login: '',
  password: '',
  token: ''
};

export const auth = () => async (dispatch, getState) => {
  const { authReducer: { login, password } } = getState();
  try {
    const r = await api.getAuthToken(login, password);
    if(r.status === 200) dispatch(authActions.setAuth(true, r.data.access_token))
  } catch (error) {
    alert('Неверный логин или пароль');
  }
}

const setLogin = (state, login) => {
  return {
    ...state,
    login
  }
}

const setPassword = (state, password) => {
  return {
    ...state,
    password
  }
}

const authReducer = (state = inicialState, action) => {
  switch(action.type){
    case authAT.SET_AUTH:
      return {
        ...state,
        isAuth: action.isAuth,
        token: action.token
      }

    case authAT.SET_LOGIN:
      return setLogin(state, action.login)

    case authAT.SET_PASSWORD:
      return setPassword(state, action.password)

    default: return state;
  }
}

export default authReducer;