import { chartAT } from './../actions-types';
import { chartActions } from './../actions';
import BankApi from './../../api/bank-api';
import { chartFilterValidator } from './../../validators';

const bankApi = new BankApi();

const inicialState = {
  minDate: '2000-01-01',
  maxTimeSpan: 31536000000, //1 year in ms
  startDate: '',
  endDate: '',
  selectedCurrencyId: '',
  courseDynamicData: []
};

export const getCourseDynamicData = () => async (dispatch, getState) => {
  const { chartReducer: { minDate, maxTimeSpan, startDate, endDate, selectedCurrencyId } } = getState();
  try {
    if(chartFilterValidator(minDate, maxTimeSpan, startDate, endDate, selectedCurrencyId)){
      const dynamicData = await bankApi.getCurrencyDynamic(startDate, endDate, selectedCurrencyId);
      if(dynamicData.status === 200) dispatch(chartActions.setCourseDynamicData(dynamicData.data))
    }
  } catch (error) {
    if(error.message === 'Network Error'){
      alert('Отсутствует подключение к интернету!');
    }
  }
}

const setStartDate = (state, startDate) => {
  return {
    ...state,
    startDate
  }
}

const setEndDate = (state, endDate) => {
  return {
    ...state,
    endDate
  }
}

const setSelectedCurrencyChartFilter = (state, value) => {
  return {
    ...state,
    selectedCurrencyId: value
  }
}

const chartReducer = (state = inicialState, action) => {
  switch(action.type){
    case chartAT.SET_COURSE_DYNAMIC_DATA:
      return {
        ...state,
        courseDynamicData: action.data
      }

    case chartAT.SET_START_DATE:
      return setStartDate(state, action.startDate)

    case chartAT.SET_END_DATE:
      return setEndDate(state, action.endDate)

    case chartAT.SET_SELECTED_CURRENCY_CHART_FILTER:
      return setSelectedCurrencyChartFilter(state, action.value)

    default: return state;
  }
}

export default chartReducer;