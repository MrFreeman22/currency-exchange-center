import { receivingCurrencyAT } from './../actions-types';
import { receivingCurrencyActions } from './../actions';
import { isNumber } from './../../validators';
import Api from './../../api/api';

const api = new Api();

const inicialState = {
  selectedCurrencyId: '',
  amountAll: '',
  amountAdd: '',
  apiMethod: ''
};


const setAmountAdd = (state, amountAdd) => {
  if(isNumber(+amountAdd)) return {...state, amountAdd}
  return state;
}

export const fillIn = () => async (dispatch, getState) => {
  
  const { receivingCurrencyReducer } = getState();
  const { apiMethod, amountAdd, amountAll, selectedCurrencyId } = receivingCurrencyReducer;
  const amount = Number(amountAll) + Number(amountAdd);

  try {
    const r = await api.setAmount(apiMethod, +amount, +selectedCurrencyId);
    if(r.status === 204 || r.status === 201) {//no content or created
      dispatch(setSelecteCurrencyId(selectedCurrencyId));
      dispatch(receivingCurrencyActions.setApiMethod('put'));
      dispatch(receivingCurrencyActions.setAmountAdd(''));
      alert('Пополнение прошло успешно!')
    } 
  } catch (error) {
    alert(error);
  }
}



export const setSelecteCurrencyId = selectedCurrencyId => async dispatch =>  {
  dispatch(receivingCurrencyActions.setSelecteCurrencyId(selectedCurrencyId));
  try {
    const r = await api.getAmount(selectedCurrencyId);
    if(r.status === 200) {
      dispatch(receivingCurrencyActions.setApiMethod('put'));
      dispatch(receivingCurrencyActions.setAmountAll(r.data.amount))
    } 
  } catch (error) {
    if(error.response.status === 404) {
      dispatch(receivingCurrencyActions.setApiMethod('post'));
      dispatch(receivingCurrencyActions.setAmountAll(0));
    } 
  }
}

const receivingCurrencyReducer = (state = inicialState, action) => {
  switch(action.type){

    case receivingCurrencyAT.SET_SELECTED_CURRENCY_ID:
      return {...state, selectedCurrencyId: action.selectedCurrencyId}

    case receivingCurrencyAT.SET_AMOUNT_ALL:
      return {...state, amountAll: action.amountAll}

    case receivingCurrencyAT.SET_AMOUNT_ADD:
      return setAmountAdd(state, action.amountAdd)

    case receivingCurrencyAT.SET_API_METHOD:
      return {...state, apiMethod: action.apiMethod}

    default: return state;
  }
}

export default receivingCurrencyReducer;