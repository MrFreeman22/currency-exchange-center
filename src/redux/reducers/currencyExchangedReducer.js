import { currencyExchangedAT } from './../actions-types';
import { currencyExchangeValidator } from './../../validators';
import { currencyExchangeActions } from './../actions';
import Api from './../../api/api';

const api = new Api();

const inicialState = {
  acceptedCurrencyId: '',//id валюты которую хотят купить
  acceptedCurrencyAmount: '',//сколько сдаем на обмен
  availableForExchange: '',//доступно для обмена

  soldCurrencyAmount: '',//сколько получаем(считает автоматом)
  
  isAvailableAcceptedCurrencyAmountInput: false,//заблокировать инпут пока не получим availableForExchange с сервера
  apiMethod: '',
  maxBYN: '',
  vis: true,
};


const f = (dispatch, amount, isAvailable) => {
  dispatch(currencyExchangeActions.setAvailableForExchange(amount));
  dispatch(currencyExchangeActions.setIsAvailableAcceptedCurrencyAmountInput(isAvailable))
}

export const getCurrencyAmount = acceptedCurrencyId => async dispatch =>  {
  dispatch(currencyExchangeActions.setAcceptedCurrencyId(acceptedCurrencyId));

  try {
    const r = await api.getAmount(acceptedCurrencyId);
    if(r.status === 200) f(dispatch, r.data.amount, true)

  } catch (error) {
    if(error.response.status === 404) f(dispatch, 0, true) 
  }
}


const setAcceptedCurrencyAmount = (state, amount) => {
  if(currencyExchangeValidator(amount, state.availableForExchange)){
    return {...state, acceptedCurrencyAmount: amount}
  }
  return state;
}

export const exchange = () => async (dispatch, getState) => {
  const { currencyExchangeReducer } = getState();
  const { acceptedCurrencyId, acceptedCurrencyAmount, availableForExchange, soldCurrencyAmount } = currencyExchangeReducer;
  
  const amount = Number(availableForExchange) - Number(acceptedCurrencyAmount);
  const method = 'put';
  dispatch(currencyExchangeActions.setVis(false));
  try {
    const byn = await api.getAmount(1);
    if(byn.status === 200) {

      const r = await api.setAmount(method, +amount, +acceptedCurrencyId);
      if(r.status === 204) {

        const amountBYN = Number(byn.data.amount) + Number(soldCurrencyAmount);
        const res = await api.setAmount(method, amountBYN, 1);//byn
        if(res.status === 204) {

          dispatch(getCurrencyAmount(acceptedCurrencyId));
          dispatch(currencyExchangeActions.setAcceptedCurrencyAmount(''));
          dispatch(currencyExchangeActions.setSoldCurrencyAmount(''));
          alert('Обмен прошел успешно!');
          dispatch(currencyExchangeActions.setVis(true));
        }
      } 
    }
  } catch (error) {
    console.dir(error);
    alert(error)
  }
}

const currencyExchangedReducer = (state = inicialState, action) => {
  switch(action.type){

    case currencyExchangedAT.SET_ACCEPTED_CURRENCY_ID: 
      return {...state, acceptedCurrencyId: action.acceptedCurrencyId}

    case currencyExchangedAT.SET_IS_AVAILABLE_ACCEPTED_CURRENCY_AMOUNT_INPUT: 
      return {...state, isAvailableAcceptedCurrencyAmountInput: action.isAvailable}

    case currencyExchangedAT.SET_SOLD_CURRENCY_AMOUNT: 
      return {...state, soldCurrencyAmount: action.soldCurrencyAmount}

    case currencyExchangedAT.SET_ACCEPTED_CURRENCY_AMOUNT: 
      return setAcceptedCurrencyAmount(state, action.amount)

    case currencyExchangedAT.SET_AVAILABLED_FOR_EXCHANGE: 
      return {...state, availableForExchange: action.availableForExchange}

    case currencyExchangedAT.SET_API_METHOD:
      return {...state, apiMethod: action.apiMethod}

    case currencyExchangedAT.SET_MAX_BYN:
      return {...state, maxBYN: action.maxBYN}

    case currencyExchangedAT.SET_VIS:
      return {...state, vis: action.vis}

    default: return state;
  }
}

export default currencyExchangedReducer;