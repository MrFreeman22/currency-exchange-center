import { combineReducers } from 'redux';
import chartReducer from './chartReducer';
import currenciesReducer from './currenciesReducer';
import authReducer from './authReducer';
import receivingCurrencyReducer from './receivingCurrencyReducer';
import currencyExchangeReducer from './currencyExchangedReducer';

export default combineReducers({
  chartReducer,
  currenciesReducer,
  authReducer,
  receivingCurrencyReducer,
  currencyExchangeReducer
});
