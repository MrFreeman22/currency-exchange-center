import { receivingCurrencyAT } from './../actions-types';

export const setSelecteCurrencyId = selectedCurrencyId => {
  return {
    type: receivingCurrencyAT.SET_SELECTED_CURRENCY_ID,
    selectedCurrencyId
  }
}

export const setAmountAll = amountAll => {
  return {
    type: receivingCurrencyAT.SET_AMOUNT_ALL,
    amountAll
  }
}

export const setAmountAdd = amountAdd => {
  return {
    type: receivingCurrencyAT.SET_AMOUNT_ADD,
    amountAdd
  }
}

export const setApiMethod = apiMethod => {
  return {
    type: receivingCurrencyAT.SET_API_METHOD,
    apiMethod
  }
}


