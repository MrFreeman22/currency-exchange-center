import { currencyExchangedAT } from './../actions-types';

export const setAcceptedCurrencyId = acceptedCurrencyId => {
  return {
    type: currencyExchangedAT.SET_ACCEPTED_CURRENCY_ID,
    acceptedCurrencyId
  }
}

export const setAcceptedCurrencyAmount = amount => {
  return {
    type: currencyExchangedAT.SET_ACCEPTED_CURRENCY_AMOUNT,
    amount
  }
}

export const setAvailableForExchange = availableForExchange => {
  return {
    type: currencyExchangedAT.SET_AVAILABLED_FOR_EXCHANGE,
    availableForExchange
  }
}

export const setSoldCurrencyAmount = soldCurrencyAmount => {
  return {
    type: currencyExchangedAT.SET_SOLD_CURRENCY_AMOUNT,
    soldCurrencyAmount
  }
}

export const setIsAvailableAcceptedCurrencyAmountInput = isAvailable => {
  return {
    type: currencyExchangedAT.SET_IS_AVAILABLE_ACCEPTED_CURRENCY_AMOUNT_INPUT,
    isAvailable
  }
}

export const setApiMethod = apiMethod => {
  return {
    type: currencyExchangedAT.SET_API_METHOD,
    apiMethod
  }
}

export const setMaxBYN = maxBYN => {
  return {
    type: currencyExchangedAT.SET_MAX_BYN,
    maxBYN
  }
}

export const setVis = vis => {
  return {
    type: currencyExchangedAT.SET_VIS,
    vis
  }
}