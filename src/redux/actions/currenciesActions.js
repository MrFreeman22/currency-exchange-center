import { currenciesAT } from './../actions-types';

export const setAllCurrencies = (currencies) => {
  return {
    type: currenciesAT.SET_ALL_CURRENCIES,
    currencies
  }
}
