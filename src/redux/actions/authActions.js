import { authAT } from './../actions-types';

export const setAuth = (isAuth, token) => {
  return {
    type: authAT.SET_AUTH,
    isAuth,
    token
  }
}

export const setLogin = (login) => {
  return {
    type: authAT.SET_LOGIN,
    login
  }
}

export const setPassword = (password) => {
  return {
    type: authAT.SET_PASSWORD,
    password
  }
}