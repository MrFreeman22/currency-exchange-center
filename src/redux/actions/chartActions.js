import { chartAT } from './../actions-types';

export const setCourseDynamicData = (data) => {
  return {
    type: chartAT.SET_COURSE_DYNAMIC_DATA,
    data
  }
}

export const setStartDate = (startDate) => {
  return {
    type: chartAT.SET_START_DATE,
    startDate
  }
}

export const setEndDate = (endDate) => {
  return {
    type: chartAT.SET_END_DATE,
    endDate
  }
}

export const setSelectedCurrencyChartFilter = (value) => {
  return {
    type: chartAT.SET_SELECTED_CURRENCY_CHART_FILTER,
    value
  }
}