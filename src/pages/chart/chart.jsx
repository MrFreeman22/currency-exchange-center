import React from 'react';
import './chart.css';
import { useSelector } from 'react-redux';
import ChartFilter from '../../components/chart-filter/chart-filter';
import MyChart from 'react-apexcharts';

const Chart = () => {

  const currencies = useSelector(({currenciesReducer}) => currenciesReducer.currencies);
  const { courseDynamicData, selectedCurrencyId } = useSelector(({chartReducer}) => chartReducer);

  const obj = currencies.filter(item => +item.Cur_ID === +selectedCurrencyId)[0];

  const config = {       
    series: [
      {
        name: 'Курс',
        data: courseDynamicData.map(d => d.Cur_OfficialRate)
      }
    ],
    options: {
      chart: {
        height: 350,
        type: 'line',
        zoom: {
          enabled: true
        }
      },
      dataLabels: {
        enabled: true
      },
      stroke: {
        curve: 'straight'
      },
      title: {
        text: `Динамика изменения курса валюты - '${obj && obj.Cur_Name}'`,
        align: 'left'
      },
      grid: {
        row: {
          colors: ['#f3f3f3', 'transparent'], 
          opacity: 0.5
        },
      },
      xaxis: {
        categories: courseDynamicData.map(d => d.Date.slice(0, d.Date.indexOf('T'))),
      }
    }
  }

  return (
    <div className='container'>
      <div className='chart-filter-container'>
        <ChartFilter />
      </div>
      <div className='chart-container'>
        {
          courseDynamicData.length > 0 &&
            <MyChart options={config.options} series={config.series} type='line' height={350} />
        }  
      </div>
    </div>
  );
}

export default Chart;
