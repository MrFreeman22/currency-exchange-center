import React from 'react';
import { Page, Text, View, Document, StyleSheet, Font } from '@react-pdf/renderer';
// import { useSelector } from 'react-redux';
// Create styles

Font.register({
  family: "Roboto",
  src:
    "https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-medium-webfont.ttf"
});

const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: '#ffffff'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  },
  myText: {
    fontFamily: 'Roboto'
  }
});

export const Check = ({ curName, rate, abbr, amount, amountBYN }) => {

  const dateNow = `${new Date().getDate()}.${(new Date().getMonth() + 1)}.${new Date().getFullYear()} `;
  const timeNow = `${new Date().getHours()}:${(new Date().getMinutes())}:${new Date().getSeconds()}`;
  return (
    <Document>
      <Page size="A4" style={styles.page}>
        <View>
          <Text style={styles.myText}>{`Валюта: ${curName}`}</Text>
        </View>
        <View>
          <Text style={styles.myText}>{`Курс: ${rate}`}</Text>
        </View>
        <View>
          <Text style={styles.myText}>{`Сдано: ${amountBYN} BYN`}</Text>
        </View>
        <View>
          <Text style={styles.myText}>{`Получено: ${amount} ${abbr}`}</Text>
        </View>
        <View>
          <Text style={styles.myText}>{`Дата: ${dateNow} ${timeNow}`}</Text>
        </View>
      </Page>
    </Document>
  );
} 
