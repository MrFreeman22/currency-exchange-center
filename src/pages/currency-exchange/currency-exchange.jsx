import React, { useState } from 'react';
import './currency-exchange.css';
import { useDispatch, useSelector } from 'react-redux';
import { currencyExchangeActions } from '../../redux/actions';
import { getAllCurrencies } from '../../redux/reducers/currenciesReducer';
import { getCurrencyAmount, exchange } from './../../redux/reducers/currencyExchangedReducer';

import { PDFDownloadLink} from '@react-pdf/renderer';
import { Check } from './check';

const calculatedValue = (amount, currency) => {
  return +((amount/currency.Cur_Scale) * currency.Cur_OfficialRate).toFixed(2)
}

const calculatedValueBYN = (amount, currency) => {
  return +((amount/currency.Cur_OfficialRate) * currency.Cur_Scale).toFixed(2)
}

const CurrencyExchange = () => {
  const dispatch = useDispatch();
  const [curName, setCurName] = useState('');

  const currencies = useSelector(({currenciesReducer}) => currenciesReducer.currencies);
  const currencyExchangeState = useSelector(({currencyExchangeReducer}) => currencyExchangeReducer);

  const { acceptedCurrencyId, acceptedCurrencyAmount, isAvailableAcceptedCurrencyAmountInput,
          soldCurrencyAmount, availableForExchange, maxBYN, vis } = currencyExchangeState;

  if(currencies.length === 0) dispatch(getAllCurrencies());
  return (
    <div className='sale-of-currency'>
      <form>
        <h3>Хотят купить</h3>  
        <div className='form-group'>
          <select className='custom-select sale-of-currency__select'
                  value={acceptedCurrencyId}
                  onChange={e => {
                    dispatch(getCurrencyAmount(e.target.value))
                  }}
          >
            <option value='' disabled>Выберите валюту</option>
            {
              currencies.map(currency => {
                return <option key={currency.Cur_ID} value={currency.Cur_ID}>{currency.Cur_Name}</option>
              })
            }
          </select>
        </div>
        <div className='form-group'>
          <input type='text'
                 className='form-control'
                 min='1' max='10000'  
                 placeholder='Введите сумму'
                 value={acceptedCurrencyAmount}
                 disabled={!isAvailableAcceptedCurrencyAmountInput}
                 onChange={e => {
                    dispatch(currencyExchangeActions.setAcceptedCurrencyAmount(e.target.value));
                    const findedItem = currencies.filter(item => +item.Cur_ID === +acceptedCurrencyId)[0];
                    setCurName(findedItem);
                    dispatch(currencyExchangeActions.setMaxBYN(calculatedValue(availableForExchange, findedItem)))
                    if(+e.target.value <= +availableForExchange) { 
                      dispatch(currencyExchangeActions.setSoldCurrencyAmount(calculatedValue(e.target.value, findedItem)))
                    }
                 }}
          />
        </div>
        <div className='form-group'>
          { 
            availableForExchange || availableForExchange === 0 
              ? `Доступно для обмена - ${availableForExchange}` 
              : 'Выберите валюту'
          }
        </div>
        <hr/>
        <h3>Покупателю необходимо сдать BYN</h3>
        <div className='form-group'>
          <input type='text'
                 className='form-control'
                 value={soldCurrencyAmount}
                 disabled={!isAvailableAcceptedCurrencyAmountInput}
                 onChange={e => {
                  if(acceptedCurrencyId && +e.target.value <= +maxBYN){
                    dispatch(currencyExchangeActions.setSoldCurrencyAmount(e.target.value));
                    const findedItem = currencies.filter(item => +item.Cur_ID === +acceptedCurrencyId)[0];
                    dispatch(currencyExchangeActions.setAcceptedCurrencyAmount(calculatedValueBYN(e.target.value, findedItem)))
                  }
                 }}    
          />
        </div>
        {
          !vis &&
          <button type='button' 
              className='btn btn-primary'
              disabled={!+availableForExchange > 0}
              >Обменять
          </button>
        }

        <div >
          {
            vis && 
              <PDFDownloadLink 
                document={
                  <Check curName={curName.Cur_Name}
                         rate={curName.Cur_OfficialRate}
                         abbr={curName.Cur_Abbreviation}
                         amount={acceptedCurrencyAmount}
                         amountBYN={soldCurrencyAmount}/>
                } 
                fileName="check.pdf">
                {
                  ({loading}) => (loading ? 'Загрузка документа...' : 
                    <button type='button' 
                      className='btn btn-primary'
                      disabled={!+availableForExchange > 0}
                      onClick={() => {
                        dispatch(exchange())
                      }}
                    >Обменять</button>
                  )
                }
              </PDFDownloadLink> 
          }
          
        </div>
      </form>
      
    </div>
  )
}

export default CurrencyExchange;