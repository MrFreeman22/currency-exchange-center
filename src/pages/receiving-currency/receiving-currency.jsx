import React from 'react';
import './receiving-currency.css';
import { useDispatch, useSelector } from 'react-redux';
import { receivingCurrencyActions } from './../../redux/actions';
import { getAllCurrencies } from './../../redux/reducers/currenciesReducer';
import { setSelecteCurrencyId, fillIn } from './../../redux/reducers/receivingCurrencyReducer';


const ReceivingCurrency = () => {
  const dispatch = useDispatch();

  const {amountAll, amountAdd, selectedCurrencyId} = useSelector(({receivingCurrencyReducer}) => receivingCurrencyReducer);
  const currencies = useSelector(({currenciesReducer}) => currenciesReducer.currencies);

  if(currencies.length === 0) dispatch(getAllCurrencies());
  return (
    <div className='receiving-currency'>
      <h3>Пополнение выбранной валюты</h3>
      <form>  
        <div className='form-group'>
          <select className='custom-select select-currency'
                  value={selectedCurrencyId}
                  onChange={e => dispatch(setSelecteCurrencyId(e.target.value))}
          >
            <option value='' disabled>Выберите валюту</option>
            <option value='1'>Белорусский рубль</option>
            {
              currencies.map(currency => {
                return <option key={currency.Cur_ID} value={currency.Cur_ID}>{currency.Cur_Name}</option>
              })
            }
            
          </select>
        </div>
        
        <div className='form-group' >
          { 
              amountAll || amountAll === 0 ? `Текущее количество - ${amountAll}` : 'Выберите валюту'     
          }  
        </div>

        <div className='form-group'>
          <input type='text'
                 className='form-control'
                 min='1' max='10000'  
                 placeholder='Введите сумму, макс. - 10000'
                 value={amountAdd || ''}
                 onChange={e => dispatch(receivingCurrencyActions.setAmountAdd(+e.target.value))} 
          />
        </div>
  
        <button type='button' 
                className='btn btn-primary'
                disabled={!+amountAdd > 0}
                onClick={() => dispatch(fillIn())}
                >Пополнить</button>
      </form>
      
    </div>
  )
}

export default ReceivingCurrency;