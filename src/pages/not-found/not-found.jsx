import React from 'react';
import './not-found.css';
import { NavLink } from 'react-router-dom';

const NotFound = () => {
  return (
    <>
      <h1 className='not-found-title'>Запрашиваемой страницы не существует!</h1>
      <NavLink to='/' className='not-found__to-main'>На главную</NavLink>
    </>
  ) 
}

export default NotFound;