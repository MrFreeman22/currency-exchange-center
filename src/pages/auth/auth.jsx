import React from 'react';
import './auth.css';
import { useDispatch, useSelector } from 'react-redux';
import { authActions } from './../../redux/actions';
import { auth } from './../../redux/reducers/authReducer';

const Auth = () => {
  const dispatch = useDispatch();
  const { login, password } = useSelector(({authReducer}) => authReducer);

  const btnDisabled = login.length > 0 && password.length > 0;
  return (
    <div className='container-auth'>
      <h2>Автоматизированное рабочее место работника валютообменного центра</h2>
      <form>
        <div className='form-group'>
          <input type='text' 
                 className='form-control' 
                 placeholder='Введите логин' 
                 maxLength='20'
                 value={login}
                 onChange={e => dispatch(authActions.setLogin(e.target.value))}/>
        </div>
        <div className='form-group'>
          <input type='password' 
                 className='form-control' 
                 placeholder='Введите пароль'
                 value={password}
                 onChange={e => dispatch(authActions.setPassword(e.target.value))}/>
        </div>
        <button type='button' 
                className='btn btn-info'
                disabled={!btnDisabled}
                onClick={() => dispatch(auth())}>Войти</button>
      </form>
    </div>
  )
}

export default Auth;