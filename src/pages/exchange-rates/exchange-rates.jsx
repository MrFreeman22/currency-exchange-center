import React, { useEffect } from 'react';
import './exchange-rates.css';
import { useDispatch, useSelector } from 'react-redux';
import { getAllCurrencies } from './../../redux/reducers/currenciesReducer';
import Table from './../../components/table';

const ExchangeRates = () => {
  const dispatch = useDispatch();
  const currencies = useSelector(({currenciesReducer}) => currenciesReducer.currencies);

  useEffect(() => {
    dispatch(getAllCurrencies());
  }, [dispatch])

  return (
    <div className='container'>
      <Table currencies={currencies} />
    </div>
  );
}

export default ExchangeRates;
