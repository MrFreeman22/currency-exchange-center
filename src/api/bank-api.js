import axios from 'axios';
//https://www.nbrb.by/api/exrates/rates/dynamics/145?startdate=2020-10-01&2020-10-10

//https://www.nbrb.by/api/exrates/rates/dynamics/145?startdate=2019.1.1&enddate=2020.9.9 - курс за период
//https://www.nbrb.by/api/exrates/rates?periodicity=0 - все валюты отноительно Бел руб
//https://www.nbrb.by/api/exrates/rates/145 - конкретная валюта
export default class BankApi {
  #ax = axios.create({
      baseURL: 'https://www.nbrb.by/api/exrates/rates'
  })

  getAllCurrencies = async () => {
    return await this.#ax.get('?periodicity=0')
  }

  getCurrency = async (id) => {
    return await this.#ax.get(`/${id}`)
  }

  getCurrencyDynamic = async (from, to, id) => {
    return await this.#ax.get(`/dynamics/${id}?startdate=${from}&enddate=${to}`)
  }

}