import axios from 'axios';

export default class Api {
  #ax = axios.create({
      baseURL: 'https://localhost:5001/'
  })

  getAuthToken = async (name, pass) => {
    return await this.#ax.post(`token?username=${name}&password=${pass}`)
  }

  getAmount = async id => await this.#ax.get(`api/Currencies/${id}`)
  
  setAmount = async (method, amount, apiId) => {
    if(method === 'post') {
      return await this.#ax.post('api/Currencies', { amount, apiId })
    }
    return await this.#ax.put(`/api/Currencies/${apiId}`, { amount, apiId })
  }

}